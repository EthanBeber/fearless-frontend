import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm  from './ConferenceForm';
import AttendeeForm from './AttendeeForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import {BrowserRouter} from 'react-router-dom';
import {Routes} from 'react-router-dom';
import {Route} from 'react-router-dom';


function App(props) {
  if (props.attendees === undefined){
    return null;
  }
  return (
    <BrowserRouter>
    <Nav />
    <div className="container">
      <Routes>
      <Route path ="attendees/new" element={<AttendeeForm />} />
      <Route path ="conferences/new" element ={ <ConferenceForm /> } />
      <Route path = "locations/new" element={ <LocationForm /> } />
      <Route path = "attendees" element={ <AttendeesList attendees={props.attendees} /> } />
      <Route path= "presentations/new" element={ <PresentationForm/>} />
      <Route index element={<MainPage />} />
      </Routes>

    </div>
    </BrowserRouter>
  )
}

export default App;
